
let main_node = document.querySelector("#quizz-app");
let chapter_id = main_node.dataset.chapterId;
let DB = DOM_Builder;

async function get_answers() {
    let response = await fetch("/quizz/" + chapter_id, {
        headers: {"Content-Type": "application/json"}
    });

    return await response.json();
}

function view_answers(answers) {
    main_node.innerHTML = '';
    if(answers.current_question >= answers.questions.length) {
        main_node.appendChild(
            DB.div({}, [
                 'You have answered all the questions',
                 DB.button(
                    {onclick: function(){ restart(answers);}},
                    ["Start over!"]
                )
            ])
        );
        return;
    }

    let question = answers.questions[answers.current_question]
    main_node.appendChild(
        DB.div({}, [
            DB.h2({}, [chapter_id]),
            DB.div({}, view_navigation(answers)),
            DB.hr({}, []),
            DB.rawInDiv({}, question.title),
            DB.form({onsubmit: function(e){ console.log(e, answers);e.preventDefault(); validate_answer(answers);}}, [
                DB.textarea({
                    placeholder: "Enter your answer here!",
                    id: "answer-input",
                    rows: 7,
                    cols: 80,
                    style: "display: block;"
                }, [question.answer === null ? "" : question.answer]),
                DB.input({
                    type: "submit",
                    value: "Validate and next question!"
                }, [])
            ])
        ])
    );
    let textarea = document.querySelector("#answer-input")
    textarea.focus();
    textarea.setSelectionRange(textarea.value.length,textarea.value.length);
}

function view_navigation(answers) {
    return Array.from({length: answers.questions.length}).map(function(x, i) {
        let border = answers.current_question == i ?
            "border: 3px #F66 solid;":"border: 3px #FDD solid;"
        return DB.span({
                onClick: function() {goto(answers, i);},
                style:  border + "margin: 5px; cursor: pointer;"
            },
            [(i + 1) + ""]
        );
    });
}


async function goto(answers, i) {
    await send_current_answer(answers);
    new_answers = await get_answers();
    new_answers.current_question = i
    view_answers(new_answers);
}

async function validate_answer(answers) {
    await send_current_answer(answers);
    new_answers = await get_answers();
    new_answers.current_question = answers.current_question + 1;
    view_answers(new_answers);
}

async function send_current_answer(answers) {
    let answer = document.querySelector("#answer-input").value;
    let question = answers.questions[answers.current_question];
    question.answer = answer;
    await fetch("/quizz/" + chapter_id, {
        method: "POST",
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(answers)
    });
}

function restart(answers) {
    answers.current_question = 0;
    view_answers(answers);
}



async function run() {
    let answers = await get_answers();
    view_answers(answers);
}

run();