import enum
import flask
import flask_login
from flask.blueprints import Blueprint
from jinja2.exceptions import TemplateNotFound

from database import db
import json

lessons = Blueprint(
    'lessons',
    __name__,
    template_folder='templates',
    static_folder='static',
)

class QuestionState(str, enum.Enum):
    DRAFT = "DRAFT"
    PUBLISHED = "PUBLISHED"
    WAITING_FOR_CORRECTION = "WAITING_FOR_CORRECTION"
    CORRECTED = "CORRECTED"


class Chapter(db.Model):
    id = db.Column(db.Text, primary_key=True)
    state = db.Column(db.Enum(QuestionState))
    _questions = db.Column(db.Text)

    @property
    def questions(self):
        return self._questions

    @questions.setter
    def questions(self, value):
        self._questions_list = sanitize_questions(value)
        self._questions = json.dumps(self._questions_list.to_dicts())

    @property
    def questions_list(self):
        try:
            return self._questions_list
        except AttributeError:
            self._questions_list = QuestionsList.from_dicts(json.loads(self.questions))
            return self._questions_list

    @questions_list.setter
    def questions_list(self, questions_list):
        self.questions = json.dumps(questions_list.to_dicts())
        self._questions_list = questions_list

    def max_score(self):
        return sum(q.coefficient for q in self.questions_list)

class FreeAnswerQuestion:
    def __init__(self, title, grade_by_answer, coefficient):
        """Grade has to be an integer between 0 and 4"""
        self.title = title
        self.grade_by_answer = grade_by_answer
        self.coefficient = coefficient

    def grade(self, answer):
        return self.grade_by_answer.get(answer.strip())

    def has_answer(self, answer):
        return answer in self.grade_by_answer

    def to_dict(self):
        return {
            'title': self.title,
            'grade_by_answer': self.grade_by_answer,
            'coefficient': self.coefficient,
        }

    def __repr__(self):
        return "[FreeAnswerQuestion: " + self.title[:10] + ", grade_by_answer:" +str(self.grade_by_answer) +"]"

    @classmethod
    def from_dict(cls, dict_):
        return cls(
            title=dict_['title'],
            grade_by_answer=dict_['grade_by_answer'],
            coefficient=float(dict_['coefficient']),
        )


class QuestionsList:

    def __init__(self, *args):
        self.questions = args

    def to_dicts(self):
        return [q.to_dict() for q in self.questions]

    @classmethod
    def from_dicts(cls, dicts):
        return cls(*(
            FreeAnswerQuestion.from_dict(d)
            for d in dicts
        ))

    def __len__(self):
        return len(self.questions)

    def __iter__(self):
        return iter(self.questions)


def sanitize_questions(questions):
    """ str -> QuestionsList. Throw various exception if illformed
     """
    for question in questions:
        if "grade_by_answer" in question:
            if type(question["grade_by_answer"]) != dict:
                raise ValueError("grade_by_answer is not a dict")

            if any(type(key) != str or value not in (0, 1, 2, 3, 4)
                for key, value in question["grade_by_answer"].items()):
                raise ValueError("grade_by_answer has to be a str -> {0,1,2,3,4} dict")
        else:
            question["grade_by_answer"] = {}
    return QuestionsList.from_dicts(questions)

@lessons.route('/chapter/', methods=['GET'])
@flask_login.login_required
def new_chapter():
    if not flask_login.current_user.is_teacher:
        return flask.render_template("error.html",
            message="Only teachers can do that!",
        ), 403

    return flask.render_template("chapter_form.html", creation=True)



@lessons.route('/create-quizz/', methods=['POST'])
@flask_login.login_required
def create_quizz():
    if not flask_login.current_user.is_teacher:
        return flask.render_template("error.html",
            message="Only teachers can do that!",
        ), 403

    chapter_id = flask.request.json

    chapter = Chapter(
        id=chapter_id,
        state=QuestionState.DRAFT,
        questions=[]
    )

    db.session.add(chapter)
    db.session.commit()

    return flask.jsonify({
        "id": chapter.id,
        "state": chapter.state,
        "questions": chapter.questions_list.to_dicts(),
    })

@lessons.route('/edit-chapter/<chapter_id>/', methods=['GET'])
@flask_login.login_required
def edit_chapter(chapter_id):
    if not flask_login.current_user.is_teacher:
        return flask.render_template("error.html",
            message="Only teachers can do that!",
        ), 403

    return flask.render_template("chapter_form.html", creation=False)

@lessons.route('/edit-chapter/<chapter_id>/get-json/', methods=['GET'])
@flask_login.login_required
def get_json(chapter_id):
    if not flask_login.current_user.is_teacher:
        return flask.jsonify({"error": "unauthorized"}), 403

    chapter = Chapter.query.get(chapter_id)
    if chapter is None:
        return flask.jsonify({"error": "chapter non existing"}), 404


    return flask.jsonify({
        "id": chapter.id,
        "state": chapter.state,
        "questions": chapter.questions_list.to_dicts(),
    })

@lessons.route('/save-quizz/<chapter_id>/', methods=['POST'])
@flask_login.login_required
def save_quizz(chapter_id):
    if not flask_login.current_user.is_teacher:
        return flask.jsonify({"error": "unauthorized"}), 403

    chapter = Chapter.query.get(chapter_id)
    if chapter is None:
        return flask.jsonify({"error": "chapter non existing"}), 404

    state = flask.request.json.get('state')
    questions = flask.request.json.get('questions')

    if None in (state, questions):
        return flask.jsonify({"error": "ill formed payload"}), 400

    try:
        chapter.questions = questions
    except Exception as e:
        return flask.jsonify({"error": "ill formed payload"}), 400

    chapter.state = state
    db.session.commit()
    return flask.jsonify("ok"), 200
    # chapter = Chapter.query.get(chapter_id)
    # if chapter is None:
    #     return flask.render_template(
    #         "error.html",
    #         message="The route %s does not exist!"%flask.request.path,
    #     )

    # if flask.request.method == 'GET':
    #     return flask.render_template(
    #         "chapter_form.html",
    #         edit=True,
    #         chapter_id=chapter_id,
    #         state=chapter.state.value,
    #         questions=chapter.questions,
    #     )

    # elif flask.request.method == 'POST':
    #     state = flask.request.form.get('state')
    #     questions = flask.request.form.get('questions', 'null')

    #     success = True
    #     if None in (state, questions):
    #         flask.flash("questions are required")
    #         success = False

    #     try:
    #         chapter.questions = questions
    #     except Exception as e:
    #         flask.flash("Something wrong happened with the question list: " + str(e))
    #         success = False

    #     if not success:
    #         return flask.render_template(
    #             "chapter_form.html",
    #             edit=True,
    #             chapter_id=chapter_id,
    #             state=state,
    #             questions=questions,
    #         )

    #     chapter.state = state
    #     db.session.commit()

    #     flask.flash("Chapter updated!")
    #    return flask.redirect("/")


@lessons.route('/export/')
@flask_login.login_required
def export():
    if not flask_login.current_user.is_teacher:
        return flask.render_template("error.html",
            message="Only teachers can do that!",
        ), 403

    return flask.jsonify([
        {
            'id': c.id,
            'state': c.state,
            'questions': json.loads(c.questions),
        }
        for c in Chapter.query.all()
    ])


def render_error(message):
    return flask.render_template(
        'error.html',
        message=message,
    ), 404
