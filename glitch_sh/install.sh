elm_bin_url="https://github.com/elm/compiler/releases/download/0.19.0/binaries-for-linux.tar.gz"

echo "Grab all python dependencies..."
pip3 install --user --upgrade pip >/dev/null
pip3 install --user  -r webapp/requirements.txt > /dev/null
echo "Python deps grabbed!"


mkdir -p .data

echo ""
if [ ! -f "/app/.local/bin/elm" ]; then
   echo "Elm doesn't seem installed, I'm downloading it..."
   cd /app/.local/bin/

   curl --silent -L $elm_bin_url  | tar xz
   echo "Elm installed!"
   cd -
else
   echo "Elm seems already installed!"
fi


if [ -z "$SECREY_KEY" ]
then
    export SECREY_KEY=`< /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c${1:-67};echo;`
    echo "SECREY_KEY=$SECREY_KEY" >> .env
    echo "SECREY_KEY created"
else
    echo "SECREY_KEY already defined"
fi


echo "Instianciate database..."
cd webapp
if ! python3 scripts/init_db.py; then
    exit;
fi
cd -
echo "Database instanciated!"

# Hack in order to discard auto-commit from glitch
echo '#!/bin/sh
if grep -q "Checkpoint$" "$1"; then
        echo "Skipping default commit" >> /tmp/message
        exit 1
fi' > /app/.git/hooks/prepare-commit-msg && chmod +x /app/.git/hooks/prepare-commit-msg

