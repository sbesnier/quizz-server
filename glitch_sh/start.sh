function build_corrector {
    elm make front/QuizzCorrector.elm --output static/quizz_corrector.js
}

function build_creator {
    elm make front/QuizzCreator.elm --output static/quizz_creator.js
}




build_creator && build_corrector && PYTHONUNBUFFERED=true python3 webapp/app.py