module QuizzCreator exposing (Mark(..), Question, Quizz, QuizzState(..))

import Array exposing (Array)
import Array.Extra as Array
import Browser
import Dict exposing (Dict)
import Element exposing (..)
import Element.Background as Background
import Element.Border as Border
import Element.Font as Font
import Element.Input as Input
import Element.Region as Region
import Html exposing (Html)
import Html.Attributes
import Html.Events
import Http
import Json.Decode as Decode exposing (Decoder, Value)
import Json.Encode as Encode


type alias Quizz =
    { id : String, questions : Array Question, state : QuizzState }


type alias Question =
    { title : String, coefficient : Float, gradeByAnswer : Dict String Mark }


type Mark
    = Mark0
    | Mark1
    | Mark2
    | Mark3
    | Mark4


type QuizzState
    = Draft
    | Published
    | WaitingForCorrection
    | Corrected


quizzStateToString : QuizzState -> String
quizzStateToString quizzState =
    case quizzState of
        Draft ->
            "DRAFT"

        Published ->
            "PUBLISHED"

        WaitingForCorrection ->
            "WAITINGFORCORRECTION"

        Corrected ->
            "CORRECTED"


markToInt : Mark -> Int
markToInt mark =
    case mark of
        Mark0 ->
            0

        Mark1 ->
            1

        Mark2 ->
            2

        Mark3 ->
            3

        Mark4 ->
            4


markDecoder : Decoder Mark
markDecoder =
    toAssocList markToInt
        [ Mark0
        , Mark1
        , Mark2
        , Mark3
        , Mark4
        ]
        |> assocToDecoder Decode.int


quizzStateDecoder : Decoder QuizzState
quizzStateDecoder =
    toAssocList quizzStateToString
        [ Draft
        , Published
        , WaitingForCorrection
        , Corrected
        ]
        |> assocToDecoder Decode.string


questionDecoder : Decoder Question
questionDecoder =
    Decode.map3 Question
        (Decode.field "title" Decode.string)
        (Decode.field "coefficient" Decode.float)
        (Decode.field "grade_by_answer" <| Decode.dict markDecoder)


quizzDecoder : Decoder Quizz
quizzDecoder =
    Decode.map3 Quizz
        (Decode.field "id" Decode.string)
        (Decode.field "questions" <| Decode.array questionDecoder)
        (Decode.field "state" quizzStateDecoder)


assocToDecoder : Decoder jsonType -> List ( jsonType, elmType ) -> Decoder elmType
assocToDecoder decoder assocList =
    decoder
        |> Decode.andThen
            (\s ->
                List.filter (Tuple.first >> (==) s) assocList
                    |> List.head
                    |> Maybe.map (Tuple.second >> Decode.succeed)
                    |> Maybe.withDefault (Decode.fail "unknown key")
            )


type alias Encoder a =
    a -> Value


quizzEncoder : Encoder Quizz
quizzEncoder quizz =
    Encode.object
        [ ( "id", Encode.string quizz.id )
        , ( "questions", Encode.array questionEncoder quizz.questions )
        , ( "state", quizzStateToString quizz.state |> Encode.string )
        ]


questionEncoder : Encoder Question
questionEncoder question =
    Encode.object
        [ ( "title", Encode.string question.title )
        , ( "coefficient", Encode.float question.coefficient )
        , ( "grade_by_answer"
          , Dict.map (always (markToInt >> Encode.int)) question.gradeByAnswer
                |> Dict.toList
                |> Encode.object
          )
        ]


type Model
    = Loading
    | QuizzOnServer Quizz
    | NewQuizz String
    | WrongId String String


type Msg
    = GotQuizz (Result Http.Error Quizz)
    | IdUpdated String
    | IdSubmitted
    | QuestionTitleUpdated Int String
    | QuestionCoefficientUpdated Int Float
    | QuestionRemoved Int
    | LastInputTitleUpdated String
    | LastInputCoefficientUpdated Float
    | StateUpdated QuizzState
    | QuizzSubmitted
    | QuizzSaved (Result Http.Error ())


init : { creation : Bool } -> ( Model, Cmd Msg )
init flag =
    if flag.creation then
        ( NewQuizz "", Cmd.none )

    else
        ( Loading, loadQuizz )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case model of
        Loading ->
            case msg of
                GotQuizz (Ok quizz) ->
                    ( QuizzOnServer quizz, Cmd.none )

                _ ->
                    let
                        _ =
                            Debug.log "Unexpected msg during loading" msg
                    in
                    ( model, Cmd.none )

        NewQuizz id ->
            case msg of
                IdUpdated newId ->
                    ( NewQuizz newId, Cmd.none )

                IdSubmitted ->
                    ( Loading, postNewQuizz id )

                _ ->
                    let
                        _ =
                            Debug.log "Unexpected msg during loading" msg
                    in
                    ( model, Cmd.none )

        WrongId wrongId id ->
            case msg of
                IdUpdated newId ->
                    ( WrongId wrongId newId, Cmd.none )

                IdSubmitted ->
                    if id == "" then
                        ( model, Cmd.none )

                    else
                        ( Loading, postNewQuizz id )

                _ ->
                    let
                        _ =
                            Debug.log "Unexpected msg during loading" msg
                    in
                    ( model, Cmd.none )

        QuizzOnServer quizz ->
            case msg of
                QuestionTitleUpdated index title ->
                    ( QuizzOnServer <| updateQuestion index title quizz, Cmd.none )

                QuestionCoefficientUpdated index coeff ->
                    ( QuizzOnServer <| updateCoeff index coeff quizz, Cmd.none )

                LastInputTitleUpdated title ->
                    ( QuizzOnServer <|
                        pushQuestionWithTitle title quizz
                    , saveQuizz quizz
                    )

                LastInputCoefficientUpdated coeff ->
                    ( QuizzOnServer <|
                        pushQuestionWithCoeff coeff quizz
                    , saveQuizz quizz
                    )

                QuestionRemoved index ->
                    let
                        newQuizz =
                            removeQuestion index quizz
                    in
                    ( QuizzOnServer <| newQuizz, saveQuizz newQuizz )

                StateUpdated state ->
                    let
                        newQuizz =
                            { quizz | state = state }
                    in
                    ( QuizzOnServer newQuizz, saveQuizz newQuizz )

                QuizzSubmitted ->
                    ( model, saveQuizz quizz )

                QuizzSaved (Ok ()) ->
                    let
                        _ =
                            Debug.log "Quizz saved!" ()
                    in
                    ( model, Cmd.none )

                _ ->
                    let
                        _ =
                            Debug.log "Unexpected msg during loading" msg
                    in
                    ( model, Cmd.none )


updateQuestion : Int -> String -> Quizz -> Quizz
updateQuestion index title quizz =
    { quizz
        | questions =
            Array.update index
                (\q -> { q | title = title })
                quizz.questions
    }


updateCoeff : Int -> Float -> Quizz -> Quizz
updateCoeff index coeff quizz =
    { quizz
        | questions =
            Array.update index
                (\q -> { q | coefficient = coeff })
                quizz.questions
    }


removeQuestion : Int -> Quizz -> Quizz
removeQuestion index quizz =
    { quizz
        | questions = Array.removeAt index quizz.questions
    }


emptyQuestion =
    { title = ""
    , coefficient = 1
    , gradeByAnswer = Dict.empty
    }


pushQuestionWithTitle : String -> Quizz -> Quizz
pushQuestionWithTitle title quizz =
    { quizz
        | questions =
            Array.push { emptyQuestion | title = title } quizz.questions
    }


pushQuestionWithCoeff : Float -> Quizz -> Quizz
pushQuestionWithCoeff coeff quizz =
    { quizz
        | questions =
            Array.push { emptyQuestion | coefficient = coeff } quizz.questions
    }


saveQuizz : Quizz -> Cmd Msg
saveQuizz quizz =
    Http.post
        { url = "/save-quizz/" ++ quizz.id ++ "/"
        , body = Http.jsonBody <| quizzEncoder quizz
        , expect = Http.expectWhatever QuizzSaved
        }


postNewQuizz : String -> Cmd Msg
postNewQuizz id =
    Http.post
        { url = "/create-quizz/"
        , body = Http.jsonBody <| Encode.string id
        , expect = Http.expectJson GotQuizz quizzDecoder
        }


loadQuizz : Cmd Msg
loadQuizz =
    Http.get { url = "get-json/", expect = Http.expectJson GotQuizz quizzDecoder }


view : Model -> Html Msg
view model =
    layout [ width fill ] <|
        case model of
            Loading ->
                text "Loading..."

            NewQuizz id ->
                viewId
                    (el [ Region.heading 2 ] <|
                        text "You are about to create a new quizz!"
                    )
                    id

            WrongId wrongId id ->
                viewId
                    (el [ Font.color <| rgb 1 0 0 ] <|
                        text "The id was rejected by the sever (note that you cannot have duplicated id."
                    )
                    id

            QuizzOnServer quizz ->
                viewQuizz quizz


viewQuizz : Quizz -> Element Msg
viewQuizz quizz =
    column [ width fill ]
        [ el [ Region.heading 2 ] <| text quizz.id
        , Input.radio []
            { onChange = StateUpdated
            , options =
                [ Input.option Draft (text "Draft")
                , Input.option Published (text "Published")
                , Input.option WaitingForCorrection (text "Correction in progress")
                , Input.option Corrected (text "Corrected")
                ]
            , label = Input.labelAbove [] (text "State:")
            , selected = Just quizz.state
            }
        , viewQuestions quizz.questions
        , Input.button [ padding 10, Background.color <| rgb 0 0 1 ]
            { label = text "Submit"
            , onPress =
                Just QuizzSubmitted
            }
        ]


viewQuestions : Array Question -> Element Msg
viewQuestions questions =
    ((questions
        |> Array.toList
        |> List.indexedMap viewQuestion
     )
        ++ [ viewEmptyQuestion ]
    )
        |> column [ width fill ]


viewQuestion : Int -> Question -> Element Msg
viewQuestion index question =
    row [ width fill ]
        [ Input.multiline []
            { spellcheck = True
            , label = Input.labelHidden "Enter the title of the question"
            , placeholder =
                Just <|
                    Input.placeholder [] <|
                        text "What the answer to life the universe and everything?"
            , onChange = QuestionTitleUpdated index
            , text = question.title
            }
        , Input.text []
            { label = Input.labelHidden "Enter the coefficient of the question"
            , placeholder =
                Just <|
                    Input.placeholder [] <|
                        text "1.25 or 1 or 5..."
            , onChange =
                String.toFloat
                    >> Maybe.withDefault 0
                    >> QuestionCoefficientUpdated index
            , text = question.coefficient |> String.fromFloat
            }
        , Input.button [ htmlAttribute <| Html.Attributes.attribute "tabindex" "-1" ]
            { label = text "x"
            , onPress =
                Just <| QuestionRemoved index
            }
        ]


viewEmptyQuestion : Element Msg
viewEmptyQuestion =
    row [ width fill ]
        [ Input.multiline [ width fill ]
            { label = Input.labelHidden "Enter the title of the question"
            , placeholder =
                Just <|
                    Input.placeholder [] <|
                        text "What the answer to life the universe and everything?"
            , onChange = LastInputTitleUpdated
            , text = ""
            , spellcheck = True
            }
        , Input.text []
            { label = Input.labelHidden "Enter the coefficient of the question"
            , placeholder =
                Just <|
                    Input.placeholder [] <|
                        text "1.25 or 1 or 5..."
            , onChange =
                String.toFloat
                    >> Maybe.withDefault 0
                    >> LastInputCoefficientUpdated
            , text = ""
            }
        ]


viewId : Element Msg -> String -> Element Msg
viewId message id =
    column [ centerY, centerX, spacing 5 ]
        [ message
        , Input.text [ onEnter IdSubmitted ]
            { onChange = IdUpdated
            , text = id
            , placeholder =
                Just <|
                    Input.placeholder [] (text "Enter the id of your quizz!")
            , label = Input.labelHidden "Enter the desired id for the quizz"
            }
        , Input.button [ padding 5, Background.color <| rgb 0 0 1 ]
            { label = text "Submit id!"
            , onPress =
                if id == "" then
                    Nothing

                else
                    Just IdSubmitted
            }
        ]


toAssocList : (a -> b) -> List a -> List ( b, a )
toAssocList fn =
    List.map (\a -> ( fn a, a ))


main =
    Browser.element
        { view = view
        , update = update
        , subscriptions = always Sub.none
        , init = init
        }


onEnter : msg -> Element.Attribute msg
onEnter msg =
    Element.htmlAttribute
        (Html.Events.on "keyup"
            (Decode.field "key" Decode.string
                |> Decode.andThen
                    (\key ->
                        if key == "Enter" then
                            Decode.succeed msg

                        else
                            Decode.fail "Not the enter key"
                    )
            )
        )
